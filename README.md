# Stock market tools for Prediction and Analysis

Mostly written in FORTRAN for efficient execution, really, similar code won't run as fast in another language. With most loops optimized for efficient memory access and parallel execution using Cuda cores.

A long time ago we used to code, run and debug in your favorite coding tool, nowadays we can appreciate the all in one plus interactivity offered by a jupyter notebook.
Therefore a good first step could be to enable the Fortran kernel for jupyter. A super easy, quick (5 lines of code) readme in this [repo](https://github.com/ZedThree/jupyter-fortran-kernel) (thanks Peter <3) will get you started with a jupyter-fortran-notebook.

P.S i have up to version 60 of fortran code, i'm not finished updating the repo